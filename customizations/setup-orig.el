(require 'mouse)
(package-initialize)
(add-to-list 'load-path
                "~/.emacs.d/elpa/yasnippet-20150415.244/")
(require 'yasnippet)
(yas-global-mode 1)

(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/elpa/auto-complete-20150618.1949/dict")
(ac-config-default)
(ac-set-trigger-key "TAB")
(ac-set-trigger-key "<tab>")


(xterm-mouse-mode t)
(defun track-mouse (e))

(add-to-list 'load-path "~/.emacs.d/ember-mode/")
(require 'ember-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;; '(ansi-color-names-vector
   ;; ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(current-language-environment "Spanish")
 ;; '(custom-enabled-themes nil)
 '(ns-right-alternate-modifier (quote none)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setq require-final-newline t)
(setq mac-option-modifier 'super
      mac-command-modifier 'meta
      x-select-enable-clipboard t)
(put 'downcase-region 'disabled nil)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
;; ensime
(add-to-list 'package-archives
	              '("melpa" . "http://melpa.milkbox.net/packages/") t)
(when (not package-archive-contents)
  (package-refresh-contents))

(setenv "PATH" (concat "/usr/local/bin/sbt" (getenv "PATH")))
(setenv "PATH" (concat "/usr/local/bin/scala" (getenv "PATH")))

(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))
(require 'ensime)
(add-hook 'scala-mode-hook 'ensime-scala-mode-hook)

;;
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

(add-hook 'haskell-mode-hook 'haskell-indentation-mode)

(require 'angular-snippets)
(eval-after-load "sgml-mode"
    '(define-key html-mode-map (kbd "C-c C-d") 'ng-snip-show-docs-at-point))

(add-hook 'js-mode-hook (lambda () (ember-mode t)))
  (add-hook 'web-mode-hook (lambda () (ember-mode t)))
